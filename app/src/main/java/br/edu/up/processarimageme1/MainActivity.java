package br.edu.up.processarimageme1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }

  public void onClickBaixar(View v){

    BaixarImageTask task = new BaixarImageTask();

    try {
      URL url = new URL("https://drive.google.com/uc?export=view&id=0BwO0-hA9fDiUbGRvSGFuaHBiT3c");
      task.execute(url);

    } catch (MalformedURLException e) {
    }
  }

  public void onClickVisualizar(View v){

    File sdcard = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
    File arquivo = new File(sdcard, "pokemon.jpg");

    try {
      FileInputStream fis = new FileInputStream(arquivo);
      Bitmap bmp = BitmapFactory.decodeStream(fis);

      ImageView imageView = (ImageView) findViewById(R.id.imageView);
      imageView.setImageBitmap(bmp);

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }


  }

  public class BaixarImageTask extends AsyncTask<URL, String, Bitmap> {

    @Override
    protected Bitmap doInBackground(URL... urls) {
      Bitmap bmp = null;

      URL url = urls[0];
      try {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        InputStream is = con.getInputStream();

        File sdCard = Environment.getExternalStoragePublicDirectory(
                                             Environment.DIRECTORY_PICTURES);
        File arquivo = new File(sdCard, "pokemon.jpg");
        FileOutputStream fos = new FileOutputStream(arquivo);
        BufferedOutputStream bos = new BufferedOutputStream(fos);


        byte[] buffer = new byte[1024];
        int qtdeByteLidos = is.read(buffer);

        while (qtdeByteLidos > 0){
          fos.write(buffer, 0 , qtdeByteLidos);
          qtdeByteLidos = is.read(buffer);
        }
        is.close();
        bos.close();
        fos.close();

      } catch (IOException e) {
        e.printStackTrace();
      }

      return bmp;
    }
  }
}









